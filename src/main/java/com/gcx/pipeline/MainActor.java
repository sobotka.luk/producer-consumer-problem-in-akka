package com.gcx.pipeline;

import akka.actor.typed.*;
import akka.actor.typed.javadsl.*;
import akka.actor.typed.receptionist.Receptionist;
import akka.actor.typed.receptionist.ServiceKey;

import java.util.stream.IntStream;

/**
 * Actor manager creates other actors.
 */
class MainActor extends AbstractBehavior<Message>  {
    // all actors in pipeline
    private ActorRef<Message> producer = null;
    private ActorRef<Message> parser;
    private ActorRef<Message> disc;
    private ActorRef<Message> sqrt;
    private ActorRef<Message> solution;
    private ActorRef<Message> performance;

    // load dispatchers from applications.conf
    private DispatcherSelector forkDispatcher = DispatcherSelector.fromConfig("fork-dispatcher");
    private DispatcherSelector threadDispatcher = DispatcherSelector.fromConfig("thread-dispatcher");
    private DispatcherSelector producerDispatcher = DispatcherSelector.fromConfig("producer-dispatcher");

    // number of actors in one group
    int maxActorsPerGroup = 30;

    /**
     * Constructor of actor manager
     * @param context akka context
     */
    private MainActor(ActorContext<Message> context) {
        super(context);
    }

    /**
     * @return instance of this class
     */
    static Behavior<Message> create() {
        return Behaviors.setup(MainActor::new);
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
            .onMessage(Setup.class, this::onStart)
            .onMessage(EndUp.class, this::onEnd)
            .onMessage(Produce.class, this::onStartProducing)
            .onMessage(LogStats.class, this::startLog)
            .onSignal(PostStop.class, signal -> onPostStop())
            .build();
    }

    /**
     * Behavior for starting printing minute stats.
     */
    private Behavior<Message> startLog(LogStats msg) {
        performance.tell(msg);
        return this;
    }

    /**
     * Send message to producer, so producer starts produce.
     */
    private Behavior<Message> onStartProducing(Produce produce) {
        if (produce != null) {
            producer.tell(new Produce());
        }
        return this;
    }

    /**
     * Setup environments
     */
    private Behavior<Message> onStart(Setup startup) {
        // create producer
        producer = createRouteGroupProducer(startup.getProductionInterval());
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("Create producer " + producer);

        // creates actors for each "black box"
        // creates new equation
        parser = createRouteGroupParser();
        // compute discriminant
        disc = createRouteGroupDisc();
        // compute square root of discriminant
        sqrt = createRouteGroupSqrt();
        // compute solution of equation
        solution = createRouteGroupSolution();
        // each minute sends stats
        performance = createRouteGroupPerformance();

        // tell actor which "black box" is next actor in pipeline
        producer.tell(new SendConsumer(parser));
        parser.tell(new SendConsumer(disc));
        disc.tell(new SendConsumer(sqrt));
        sqrt.tell(new SendConsumer(solution));
        solution.tell(new SendConsumer(performance));

        return this;
    }

    /**
     * @param end message for stopping this actor
     */
    private Behavior<Message> onEnd(EndUp end) {
        return Behaviors.stopped();
    }

    /**
     * Signal for stopping this actor
     */
    private Behavior<Message> onPostStop() {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("Main actor ends");
        return Behaviors.stopped();
    }

    /**
     * Creates pool with fixed size of producer actors and with round robin system of sending message
     * @return reference to group actor
     */
    private ActorRef<Message> createRouteRRPoolProducer(int time) {
        int poolSize = 4;
        PoolRouter<Message> pool = Routers.pool(poolSize, Behaviors.supervise(ProducerActor.create(time)).onFailure(SupervisorStrategy.restart())).withRoundRobinRouting();
        return getContext().spawn(pool, "producer-pool");
    }

    /**
     * Producers.
     * Creates group of {@link MainActor#maxActorsPerGroup} actors with round robin system of sending message
     * @return reference to group actor which produce
     */
    private ActorRef<Message> createRouteGroupProducer(int time) {
        // create service key - dispatcher is grouping actors according this one
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-router-group-Producer");

        // initialize actors - register actors into receptionist
        IntStream.range(1,maxActorsPerGroup)
                .forEach(i -> getContext().getSystem().receptionist().tell(Receptionist.register(serviceKey, getContext().spawn(ProducerActor.create(time), "Producer"+i, producerDispatcher))));

        // config group
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();
        // creates and returns group - each message send to group will be redirected to one member
        return getContext().spawn(group, "worker-group-Producer");
    }


    /**
     * Parser.
     * Creates group of {@link MainActor#maxActorsPerGroup} actors with round robin system of sending message
     * @return reference to group actor which parse equation
     */
    private ActorRef<Message> createRouteGroupParser() {
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-router-group-Parser");

        IntStream.range(1,maxActorsPerGroup).forEach(i -> getContext().getSystem().receptionist().tell(Receptionist.register(serviceKey, getContext().spawn(ParserActor.create(), "ParserActor"+i, forkDispatcher))));
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();

        return getContext().spawn(group, "worker-group-Parser");
    }

    /**
     * Computing discriminant.
     * Creates group of {@link MainActor#maxActorsPerGroup} actors with round robin system of sending message
     * @return reference to group actor which compute discriminant
     */
    private ActorRef<Message> createRouteGroupDisc() {
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-router-group-DiscriminantComputation");

        IntStream.range(1,maxActorsPerGroup).forEach(i -> getContext().getSystem().receptionist().tell(Receptionist.register(serviceKey, getContext().spawn(DiscriminantComputationActor.create(), "DiscriminantComputationActor"+i, forkDispatcher))));
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();

        return getContext().spawn(group, "worker-group-DiscriminantComputation");
    }

    /**
     * Computing square root.
     * Creates group of {@link MainActor#maxActorsPerGroup} actors with round robin system of sending message
     * @return reference to group actor which compute square root
     */
    private ActorRef<Message> createRouteGroupSqrt() {
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-router-group-DiscriminantSqrt");

        IntStream.range(1,maxActorsPerGroup).forEach(i -> getContext().getSystem().receptionist().tell(Receptionist.register(serviceKey, getContext().spawn(DiscriminantSqrtActor.create(), "DiscriminantSqrtActor"+i, forkDispatcher))));
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();

        return getContext().spawn(group, "worker-group-DiscriminantSqrt");
    }

    /**
     * Solve equation.
     * Creates group of {@link MainActor#maxActorsPerGroup} actors with round robin system of sending message
     * @return reference to group actor which print equation solution
     */
    private ActorRef<Message> createRouteGroupSolution() {
        ServiceKey<Message> serviceKey = ServiceKey.create(Message.class, "log-router-group-Solution");

        IntStream.range(1,maxActorsPerGroup).forEach(i -> getContext().getSystem().receptionist().tell(Receptionist.register(serviceKey, getContext().spawn(SolutionActor.create(), "SolutionActor"+i, forkDispatcher))));
        GroupRouter<Message> group = Routers.group(serviceKey).withRoundRobinRouting();

        return getContext().spawn(group, "worker-group-Solution");
    }

    /**
     * Stats.
     * Creates actor for printing statistic of processed messages through pipeline per second.
     */
    private ActorRef<Message> createRouteGroupPerformance() {
        return getContext().spawn(PerformanceActor.create(), "performance-actor");
    }
}
