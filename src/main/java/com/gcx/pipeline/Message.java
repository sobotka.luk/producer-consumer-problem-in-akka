package com.gcx.pipeline;

import akka.actor.typed.ActorRef;

/**
 * Message for sending consumer reference to producer
 */
public interface Message {}

/**
 * Message for transferring reference to next actor
 */
class SendConsumer implements Message {
    ActorRef<Message> consumer;

    public SendConsumer(ActorRef<Message> consumer) {
        this.consumer = consumer;
    }

    public ActorRef<Message> getConsumer() {
        return consumer;
    }
}

/**
 * Message for starting producing
 */
class Produce implements Message {
    public Produce() {}
}

/**
 * Message for stopping actors.
 */
class EndUp implements Message {
    public EndUp() {}
}

/**
 * Message for start printing statistic data.
 */
class LogStats implements Message {
    public LogStats() {}
}

/**
 * Message for creating actors.
 */
class Setup implements Message {
    final int productionInterval;

    public Setup(int productionInterval) {
        this.productionInterval = productionInterval;
    }

    public int getProductionInterval() {
        return productionInterval;
    }
}
