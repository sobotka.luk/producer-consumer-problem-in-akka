package com.gcx.pipeline;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Class represents quadratic equation.
 * Methods are called from different actors.
 */
public class Equation implements Message {
    final private String defaultEquation = "%dx^2+%dx-%d=0";
    private String equation;

    // counter of created instances
    private static int counter = 0;
    // identifier of instance
    private int id = ++counter;

    // generator of random integers
    final private Random random = new Random();
    // number which is checked for prime number
    private int prime;

    private double a, b, c, D;

    // pattern for parsing string to equation
    String patternStr = "([+-]?\\d*\\.?\\d*)x\\^2([+-]?\\d*\\.?\\d*)x([+-]?\\d*\\.?\\d*)=0";
    Pattern pattern = Pattern.compile(patternStr);

    /**
     * Creates equation as string.
     */
    public Equation() {
        this.equation = String.format(defaultEquation, id%33, id%100, id%50);
        this.prime = random.nextInt();
    }

    /**
     * Parse string equation into coefficient
     */
    public void parse() {
        Matcher matcher = pattern.matcher(equation);
        if(matcher.find()){
            a = Double.parseDouble(matcher.group(1));
            b = Double.parseDouble(matcher.group(2));
            c = Double.parseDouble(matcher.group(3));
        }
    }

    public void computeDiscriminant() {
        D = Math.pow(b, 2) - 4 * a * c;
    }
    public void squareRootDiscriminant() {
        D = Math.sqrt(D);
    }
    public double getSolutionPos() {
        return (-b+D)/(2*a);
    }
    public double getSolutionNeg() {
        return (-b-D)/(2*a);
    }

    public int getPrime() {
        return prime;
    }
    public int getId() {
        return id;
    }
    public String getEquation() {
        return equation;
    }
}
