package com.gcx.pipeline;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

// parse string into numbers
class ParserActor extends DefaultActor {

    @Override
    Behavior<Message> process(Equation data) {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Parser start ({})", name, data.getId());
        data.parse();
        receiver.tell(data);
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Parser end   ({})", name, data.getId());
        return this;
    }




    /**
     * @return instance of this class
     */
    public static Behavior<Message> create() {
        return Behaviors.setup(ParserActor::new);
    }

    /**
     * Constructor
     */
    private ParserActor(ActorContext<Message> context) {
        super(context);
    }

    static ActorRef receiver = null;

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    @Override
    Behavior<Message> receiveConsumer(SendConsumer receiver) {
        ParserActor.receiver = receiver.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }
}
