package com.gcx.pipeline;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

// compute discriminant with added time consuming operation
class DiscriminantComputationActor extends DefaultActor {

    @Override
    Behavior<Message> process(Equation data) {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Discr start ({})\t{}", name, data.getId(), data.getPrime());

        // time consuming operation
        consumeTime(data);

        data.computeDiscriminant();
        receiver.tell(data);
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Discr end   ({})", name, data.getId());
        return this;
    }

    private void consumeTime(Equation data) {
        long startTime = System.nanoTime();
        long n = (data.getPrime()>0) ? data.getPrime() : -data.getPrime();
        boolean isPrime = true;
        for(long i=2;i<n;i++) {
            if (n % i == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} isPrime ({}), {}", name, data.getId(), data.getPrime());
        }
        long endTime   = System.nanoTime();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} isPrime end ({}) {}", name, data.getId(), endTime - startTime);
    }




    /**
     * @return instance of this class
     */
    public static Behavior<Message> create() {
        return Behaviors.setup(DiscriminantComputationActor::new);
    }

    /**
     * Constructor
     */
    private DiscriminantComputationActor(ActorContext<Message> context) {
        super(context);
    }

    static ActorRef receiver = null;

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    @Override
    Behavior<Message> receiveConsumer(SendConsumer receiver) {
        DiscriminantComputationActor.receiver = receiver.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }
}
