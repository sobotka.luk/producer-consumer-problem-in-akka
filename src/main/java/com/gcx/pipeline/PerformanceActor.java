package com.gcx.pipeline;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;

import java.time.Duration;
import java.time.LocalDateTime;

// print performance with minute period
class PerformanceActor extends DefaultActor {
    static private int cnt = 0;
    private LogStats logStats= new LogStats();

    /**
     * Counts number of received messages
     */
    @Override
    Behavior<Message> process(Equation data) {
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} Performance   ({})", name, data.getId());
        cnt++;
        return this;
    }

    /**
     * each minute print average processed equations per second
     */
    @Override
    Behavior<Message> onLog(LogStats a) {
        getContext().scheduleOnce(Duration.ofSeconds(60), getContext().getSelf(), logStats);
        int act = cnt;
        cnt = 0;
        getContext().getLog().info("{} computed {} eq -> {} eq/sec", LocalDateTime.now(), act, (double)act/60);
        return this;
    }




    /**
     * @return instance of this class
     */
    public static Behavior<Message> create() {
        return Behaviors.setup(PerformanceActor::new);
    }

    /**
     * Constructor
     */
    private PerformanceActor(ActorContext<Message> context) {
        super(context);
    }

    static ActorRef receiver = null;

    /**
     * Save next reference of actor (next "black box" in pipeline)
     */
    @Override
    Behavior<Message> receiveConsumer(SendConsumer receiver) {
        PerformanceActor.receiver = receiver.getConsumer();
        if(Main.PRINT_DEBUG_LOG) getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }
}
