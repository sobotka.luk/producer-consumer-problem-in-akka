package com.gcx.basic;

import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;


/**
 * Consumer will receiving messages from producer and consume them in {@link ConsumerActor#consumeTime} milliseconds
 */
class ConsumerActor extends AbstractBehavior<Message>  {
    // consuming time
    private static int consumeTime;
    // name of actor
    String name = "";

    /**
     * Constructor of consumer
     * @param context akka context
     * @param consumeTime time of consuming one data message
     */
    private ConsumerActor (ActorContext<Message> context, int consumeTime) {
        super(context);
        ConsumerActor.consumeTime = consumeTime;
        context.getLog().info(name + " constructor");
        name = context.getSelf().toString().replaceAll(".*/", "").replaceAll("#.*", "");
    }

    /**
     * @return instance of this class
     */
    public static Behavior<Message> create(int consumeTime) {
        return Behaviors.setup(context -> new ConsumerActor(context, consumeTime));
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
                .onMessage(ProducedData.class, this::consume)
                .onSignal(PostStop.class, signal -> onPostStop())
                .build();
    }

    /**
     * @param data message with data
     */
    private Behavior<Message> consume(ProducedData data) {
        getContext().getLog().info("{} Consumer start  ({}) ", name, data.getId());
        try {
            // for simulation
            Thread.sleep(consumeTime);
        } catch (InterruptedException e) {
            getContext().getLog().error(e.getLocalizedMessage());
        }
        getContext().getLog().info("{} Consume data   ({}) ", name, data.getId());
        return this;
    }

    /**
     * Signal for stopping this actor
     */
    private Behavior<Message> onPostStop() {
        getContext().getLog().info("{} Consumer actor stopped", name);
        return Behaviors.stopped();
    }
}
