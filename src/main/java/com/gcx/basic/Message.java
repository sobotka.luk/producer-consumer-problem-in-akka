package com.gcx.basic;

import akka.actor.typed.ActorRef;

public interface Message {}

/**
 * Message for sending consumer reference to producer
 */
class SendConsumer implements Message {
    private final ActorRef<Message> consumer;

    public SendConsumer(ActorRef<Message> consumer) {
        this.consumer = consumer;
    }

    public ActorRef<Message> getConsumer() {
        return consumer;
    }
}

/**
 * Message for sending consumer reference to producer
 */
class Produce implements Message {
    ActorRef<Message> consumer;

    public Produce(ActorRef<Message> consumer) {
        this.consumer = consumer;
    }

    public ActorRef<Message> getConsumer() {
        return consumer;
    }

    public void setConsumer(ActorRef<Message> consumer) {
        this.consumer = consumer;
    }
}

/**
 * Message for transferring data from producer to consumer
 */
class ProducedData implements Message {
    private int data;
    private int id;

    public ProducedData(int id, int data){
        this.data = data;
        this.id = id;
    }
    public int getData() {
        return data;
    }

    public int getId() {
        return id;
    }
}

/**
 * Message for stopping actor.
 */
class EndUp implements Message {
    public EndUp() {}
}

/**
 * Message for creating actor.
 */
class Setup implements Message {
    final int sendInterval;
    final int consumeTime;

    public Setup(int sendInterval, int consumeTime) {
        this.sendInterval = sendInterval;
        this.consumeTime = consumeTime;
    }

    public int getSendInterval() {
        return sendInterval;
    }

    public int getConsumeTime() {
        return consumeTime;
    }
}
