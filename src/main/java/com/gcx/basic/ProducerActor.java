package com.gcx.basic;


import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.time.Duration;
import java.util.Random;


/**
 * Producer will send message to consumer each {@link ProducerActor#productionInterval} milliseconds
 */
class ProducerActor extends AbstractBehavior<Message> {
    // production interval
    static int productionInterval;
    // reference to consumer
    private ActorRef consumer;
    // name of actor
    private final String name;

    /**
     * Constructor of producer
     * @param context akka context
     * @param productionInterval production interval
     */
    private ProducerActor(ActorContext<Message> context, int productionInterval) {
        super(context);
        ProducerActor.productionInterval = productionInterval;
        name = context.getSelf().toString().replaceAll(".*/", "").replaceAll("#.*", "");
        context.getLog().info("{} constructor", name);
    }

    /**
     * @return instance of this class
     */
    public static Behavior<Message> create(int consumeTime) {
        return Behaviors.setup(context -> new ProducerActor(context, consumeTime));
    }

    /**
     * Message handling
     */
    @Override
    public Receive<Message> createReceive() {
        return newReceiveBuilder()
                .onMessage(Produce.class, this::sendToConsumer)
                .onMessage(SendConsumer.class, this::receiveConsumer)
                .onSignal(PostStop.class, signal -> onPostStop())
                .build();
    }

    // generator random numbers
    private Random random = new Random();
    // internal counter of sended messages
    private int counter = 0;

    /**
     * @param consumer message with consumer reference
     */
    private Behavior<Message> receiveConsumer(SendConsumer consumer) {
        this.consumer = consumer.getConsumer();
        getContext().getLog().info("{} receiveConsumer", name);
        return this;
    }

    /**
     * @param produce message start producing data for consumer in {@link ProducerActor#productionInterval} interval
     */
    private Behavior<Message> sendToConsumer(Produce produce) {
        // TODO: should be used
        // set up next production
        getContext().scheduleOnce(Duration.ofMillis(productionInterval), getContext().getSelf(), produce);

        int val = random.nextInt();
        getContext().getLog().info("{} sendToConsumer ({}) {}", name, counter, val);
        produce.getConsumer().tell(new ProducedData(counter++, val));
        return this;
    }

    /**
     * Signal for stopping this actor
     */
    private Behavior<Message> onPostStop() {
        getContext().getLog().info("{} Producer actor {} stopped", name, productionInterval);
        return Behaviors.stopped();
    }
}